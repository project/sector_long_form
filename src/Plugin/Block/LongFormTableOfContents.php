<?php

namespace Drupal\sector_long_form\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\sector_long_form\SectorLongFormInterface;
use Drupal\toc_api\Entity\TocType;

/**
 * Provides a 'Sector Long-form table of contents' block.
 *
 * @Block(
 *   id = "sector_long_form_toc",
 *   admin_label = @Translation("Sector › Long form table of contents"),
 *   category = @Translation("Sector"),
 * )
 */
class LongFormTableOfContents extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {

    $toc_type = TocType::load('sector_toc');
    $toc_type_options = $toc_type->getOptions();

    return [
        '#theme' => 'long_form_toc',
        //'#display' => $initial_display,
        '#utility_classes' => [
            SectorLongFormInterface::TOC_CLASS,
        ],
        '#title' => $toc_type_options['title'],
        '#attached' => [
          'library' => 'sector_long_form/app',
          'drupalSettings' => [
            'sector_long_form' => [
              'toc_class' => SectorLongFormInterface::TOC_CLASS,
              'toc_min' => $toc_type_options['header_min'],
              'toc_max' => $toc_type_options['header_max'],
            ],
          ]
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Vary cache by the 'my_param' query string parameter.
    return ['url.query_args:full', 'url.query_args:paged'];
  }

}

