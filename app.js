/**
 * Enable full page view or in-page pagination of sections.
 */

(function () {
    'use strict';

    /**
     * A pager for the long form document.
     */
    Drupal.behaviors.long_form = {
      log: (message) => {
        const prefix = 'sector_long_form: ';
        const debug = true;

        if (debug) {
          if (typeof message === 'object') {
            console.log(prefix, message);
          }
          else {
            console.log(prefix + message);
          }
        }
      },
      attach: (context, settings) => {

        const { sector_long_form: config } = settings;

        let pagingIsEnabled = true;
        // Check number of sections.
        //const numSections = $(`.${config.chunker_class} h2`).length;

        // Look for ?full parameter in URL.
        let params = new URLSearchParams(location.search);

        const prose = document.querySelector('.prose');
        const pager = document.querySelector(`.${config.pager_class}`);
        const actions = document.querySelector(`.${config.actions_class}`);
        const chunks = document.querySelectorAll(`.${config.chunker_class}`)

        if (params.has('full')) {
          pagingIsEnabled = false;
          Drupal.behaviors.long_form.log('param=full, so pagingIsEnabled is ' + pagingIsEnabled);
          pager.setAttribute('aria-hidden', true);
        }
        else if (chunks.length >= 2) {
          pagingIsEnabled = true;
          Drupal.behaviors.long_form.log('param!=full, so pagingIsEnabled is ' + pagingIsEnabled);
          pager.setAttribute('aria-hidden', false);
        }
        else {
          pagingIsEnabled = false;
          Drupal.behaviors.long_form.log('param!=full and numSections=' + chunks.length + ', so pagingIsEnabled is ' + pagingIsEnabled);
        }


        function multipageLocationHashChange() {
          var hash = location.hash;
          Drupal.behaviors.long_form.log("multipageLocationHashChange: hash=" + hash);

          const hash_elem = document.querySelector(hash);

          //const chunks2 = $(`.${config.chunker_class}`);
          //const hash_elem2 = $(`${hash}`, chunks)

          // If hash is in a section, make sure that section is visible.
          // Are we in paged mode via pagingIsEnabled? If not paged, ignore.
          if (pagingIsEnabled && hash_elem) {
            // Do we have a hit of a fragment inside a chunker section?
            /*hash_elem2.each(function () {
              Drupal.behaviors.long_form.log('match for hash inside chunker-section');
              // Get element by id, find parent section, make it visible.
              $(hash).parents(chunks2).attr('aria-hidden', false).siblings(`.${config.chunker_class}`).attr('aria-hidden', true);
              Drupal.behaviors.long_form.log('section to show is ' + $(hash).html());
            });*/
            Drupal.behaviors.long_form.log('match for hash inside chunker-section');
            const chunk = hash_elem.closest(`.${config.chunker_class}`)
            chunks.forEach(chunk => chunk.setAttribute('aria-hidden', true))
            chunk.setAttribute('aria-hidden', false);
          }

          // Add fragment to href in action buttons so we keep current viewing place.
          actions.querySelectorAll(`a`).forEach(a => a.setAttribute('href', a.getAttribute('href').split('#').at(0) + hash))
        }

        window.addEventListener("hashchange", multipageLocationHashChange, false);

        // On page load, do we have a URL fragment?
        // e.g. arriving from bookmark, or refresh existing page?
        if (location.hash) {
          Drupal.behaviors.long_form.log('detected hash in url, triggering local hashchange event' + location.hash)
          multipageLocationHashChange();
        }

        if (prose && pagingIsEnabled) {

          // Hide all sections other than first.
          if(!location.hash) {
            Drupal.behaviors.long_form.log('no url fragment, start on section 0, hide others');
            /*$(`.${config.chunker_class}`).each(function (e) {
              if (e !== 0) {
                $(this).attr('aria-hidden', true);
              }
            });*/
            [...chunks].slice(1).forEach(chunk => chunk.setAttribute('aria-hidden', true));
          }
        }
      }
    };

  })(drupalSettings);
