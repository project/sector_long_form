<?php
/**
 * @file
 * Install, update and uninstall functions for the profilename install profile.
 */
use Drupal\block\Entity\Block;
use Drupal\Core\Entity\EntityInterface;
use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\sector\SectorInstallHelpers;
use Drupal\user\Entity\Role;

/*
 * Implements hook_install()
 */
function sector_long_form_install() {
    \Drupal::service('module_installer')->install([
      'schema_article',
      'metatag_twitter_cards'
    ], FALSE);

    $administrator_role_object = Role::load('content_administrator');
    if($administrator_role_object) {
        $administrator_role_object->grantPermission('delete any sector_long_form content');
        $administrator_role_object->grantPermission('revert sector_long_form revisions');
        $administrator_role_object->grantPermission('view sector_long_form revisions');
	    $administrator_role_object->grantPermission('view any unpublished sector_long_form content');

        $administrator_role_object->save();
    }

    $editor_role_object = Role::load('content_editor');
    if($editor_role_object) {
        $editor_role_object->grantPermission('create sector_long_form content');
        $editor_role_object->grantPermission('delete own sector_long_form content');
        $editor_role_object->grantPermission('delete sector_long_form revisions');
        $editor_role_object->grantPermission('edit own sector_long_form content');
        $editor_role_object->grantPermission('edit any sector_long_form content');
	    $editor_role_object->grantPermission('view any unpublished sector_long_form content');

        $editor_role_object->save();
    }

    $moderator_role_object = Role::load('content_moderator');
    if($moderator_role_object) {
        $moderator_role_object->grantPermission('delete any sector_long_form content');
        $moderator_role_object->grantPermission('revert sector_long_form revisions');
        $moderator_role_object->grantPermission('view sector_long_form revisions');
	    $moderator_role_object->grantPermission('view any unpublished sector_long_form content');

        $moderator_role_object->save();
    }

    SectorInstallHelpers::buildSearchIndex('sector_long_form');

    // prepare sector_basic_html
    $prepare_text_formats = [ 'sector_basic_html' ];

    foreach($prepare_text_formats as $format_id) {
        $editor = Editor::load($format_id);

        if(!$editor) {
            return;
        }

        $settings = $editor->getSettings();

        $settings['plugins']['ckeditor5_sourceEditing']['allowed_tags'] = [
            ...$settings['plugins']['ckeditor5_sourceEditing']['allowed_tags'],
            '<h2 id aria-*>',
            '<h3 id aria-*>',
            '<h4 id aria-*>',
            '<h5 id aria-*>',
            '<div class aria-*>',
        ];

        $editor->setSettings($settings);
        $editor->save();

        $text_format = FilterFormat::load($format_id);

        if ($text_format->filters()->has('filter_html')) {
            // Retrieve the current filter settings.
            $filters = $text_format->get('filters');

            $new_allowed_html = '<h2 id aria-*> <h3 id aria-*> <h4 id aria-*> <h5 id aria-*> <div class aria-*>';
            $filters['filter_html']['settings']['allowed_html'] = $filters['filter_html']['settings']['allowed_html'] . $new_allowed_html;

            $text_format->set('filters', $filters);
            $text_format->save();
        }
    }

    \Drupal::service('cache.render')->invalidateAll();
    \Drupal::service('cache.entity')->invalidateAll();
}

/*
 * Implements hook_uninstall()
 */
function sector_long_form_uninstall() {

    // Delete all nodes of given content type.
    $storage_handler = \Drupal::entityTypeManager()->getStorage('node');
    $nodes = $storage_handler->loadByProperties(['type' => 'sector_long_form']);
    $storage_handler->delete($nodes);

    // Delete view
    \Drupal::service('config.factory')->getEditable('views.view.sector_long_form')->delete();

    // delete menu item
    $menu_link_content = \Drupal::service('entity.repository')->loadEntityByUuid('menu_link_content', 'x654d01d-264f-4520-b823-163c7cefd722');
    if($menu_link_content) {
        $menu_link_content->delete();
    }

    // delete view page
    $view_page = \Drupal::service('entity.repository')->loadEntityByUuid('node', 'xd52aa52-3100-4fba-b937-2079f24b8690');
    if($view_page) {
        $view_page->delete();
    }

    // Delete content type.
    $content_type = \Drupal::entityTypeManager()->getStorage('node_type')->load('sector_long_form');
    $content_type->delete();
}

/**
 * Implements hook_ENTITY_TYPE_insert() for node entities.
 */
function sector_long_form_node_insert(EntityInterface $entity) {

    switch ($entity->uuid()) {
        case 'xd52aa52-3100-4fba-b937-2079f24b8690':

            $block = Block::load('sector_theme_views_block__sector_long_form_resources_page_block');
            if(!$block) {
              return;
            }

            $block->setVisibilityConfig('request_path', ['id' => 'request_path',
                'pages'=> '/node/' . $entity->id(),
                'negate' => false,
            ]
            );
            $block->save();

            $activeThemeName = \Drupal::config('system.theme')->get('default');
            $new_id = str_replace('sector', $activeThemeName, $block->get('id'));

            $child_block = $block->createDuplicateBlock($new_id, $activeThemeName);
            $child_block->save();

            \Drupal::logger('sector_long_form')->notice('Setting sector_theme_views_block__sector_long_form_resources_page_block block visibility to appear on #%nid', [
            '%nid' => $entity->id(),
            ]);
        break;
    }
}

/**
 * Implements hook_ENTITY_TYPE_insert() for node entities.
 */
function sector_long_form_entity_insert(EntityInterface $entity) {

    switch ($entity->uuid()) {
        case '0b8ea3fe-9ed0-49e2-aac0-47d051c1a553':
        case '692e49e4-5cb6-4c08-b021-489b826984d7':

            $block = Block::load($entity->id());
            if(!$block) {
              return;
            }

            $activeThemeName = \Drupal::config('system.theme')->get('default');
            $new_id = str_replace('sector', $activeThemeName, $block->get('id'));

            $child_block = $block->createDuplicateBlock($new_id, $activeThemeName);
            $child_block->save();
        break;
    }
}
