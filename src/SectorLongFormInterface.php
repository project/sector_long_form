<?php

namespace Drupal\sector_long_form;

interface SectorLongFormInterface
{

  /**
   * @var string SectorLongFormInterface::CHUNKER_CLASS
   */
  public const CHUNKER_CLASS = 'long-form__section';

  /**
   * @var string SectorLongFormInterface::PAGER_CLASS
   */
  public const PAGER_CLASS = 'long-form__pager';

  /**
   * @var array SectorLongFormInterface::ACTIONS_CLASS
   */
  public const ACTIONS_CLASS = 'long-form__actions';

  /**
   * @var array SectorLongFormInterface::ACTIONS_CLASS
   */
  public const TOC_CLASS = 'long-form__toc';


}
