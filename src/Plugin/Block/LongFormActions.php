<?php

namespace Drupal\sector_long_form\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\sector_long_form\SectorLongFormInterface;

/**
 * Provides a 'Sector Long-form actions' block.
 *
 * @Block(
 *   id = "sector_long_form_actions",
 *   admin_label = @Translation("Sector Long-form Actions"),
 *   category = @Translation("Sector"),
 * )
 */
class LongFormActions extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $initial_display = 'disabled';
    $current_request = \Drupal::request();

    if ($current_request->query->has('paged')) {
      $initial_display = 'paged';
    }
    if ($current_request->query->has('full')) {
      $initial_display = 'full';
    }

    return [
        '#theme' => 'long_form_actions',
        '#display' => $initial_display,
        '#utility_classes' => [
            SectorLongFormInterface::ACTIONS_CLASS,
        ],
        '#attached' => [
          'library' => 'sector_long_form/app',
          'drupalSettings' => [
            'sector_long_form' => [
              'chunker_class' => SectorLongFormInterface::CHUNKER_CLASS,
              'actions_class' => SectorLongFormInterface::ACTIONS_CLASS,
            ],
          ]
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Vary cache by the 'my_param' query string parameter.
    return ['url.query_args:full', 'url.query_args:paged'];
  }

}

