(function () {
    'use strict';

    /**
     * A pager for the long form document.
     */
    Drupal.behaviors.long_form_actions = {
      attach: (context, settings) => {

        const { sector_long_form: config } = settings;
        const actions = document.querySelector(`.${config.actions_class}`);

        actions.querySelector('.long-form-actions__print').addEventListener('click', () => window.print())
      }
    };

  })();
